Novaculits = [8, 24, 32.5, 63.5, 159.5, 190.5]
Keokuk = [8, 24, 32.5, 46, 142, 155.5]
Kay_County = [8, 16, 27.25, 99.25]
Horse_Creek_Chert = [8, 16, 28]
Mookaite_Mook_Jasper = [8, 24]
Withlachoockee_Coral_GA = [4, 8, 12]
Withlachoockee_Coral_FL = [4, 14, 18]

schedule_list = [Novaculits,
                 Keokuk,
                 Kay_County,
                 Horse_Creek_Chert,
                 Mookaite_Mook_Jasper,
                 Withlachoockee_Coral_GA,
                 Withlachoockee_Coral_FL]

schedule_name_list = ['Novaculits',
                 'Keokuk',
                 'Kay County',
                 'Horse Creek Chert',
                 'Mookaite Mook Jasper',
                 'Withlachoockee Coral GA',
                 'Withlachoockee Coral FL']